require 'rails_helper'

RSpec.describe 'Api::V1::Authentications', type: :request do
  describe 'POST #signup' do
    context 'with valid params' do
      it 'register a new user' do
        post '/api/v1/signup',
             params: { email: 'test@example.com', password: 'password', password_confirmation: 'password' }

        expect(response).to have_http_status(:created)
        expect(json['token']).not_to be_nil
        expect(json['user']['email']).to eq('test@example.com')
      end
    end

    context 'with invalid params' do
      it 'return an error' do
        post '/api/v1/signup',
             params: { email: 'test@example.com', password: 'password', password_confirmation: 'wrong' }
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe 'POST #login' do
    let!(:user) { User.create(email: 'login@example.com', passwordP: 'password', password_confirmation: 'password') }

    context 'with valid credentials' do
      it 'returns a token' do
        post '/api/v1/login', params: { email: user.email, password: 'password' }

        expect(response).to have_http_status(:ok)
        expect(json['token']).not_to be_nil
      end
    end

    context 'with invalid credentials' do
      it 'returns an error' do
        post '/api/v1/login', params: { email: user.email, password: 'wrongpassword' }

        expect(response).to have_http_status(:unauthorized)
        # Your specific error response handling here
      end
    end
  end

  def json
    JSON.parse(response.body)
  end
end
