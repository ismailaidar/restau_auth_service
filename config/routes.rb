Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      post 'signup', to: 'authentication#signup'
      post 'login', to: 'authentication#login'
      post 'logout', to: 'authentication#logout'
    end
  end
end
