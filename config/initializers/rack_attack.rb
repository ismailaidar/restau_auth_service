class Rack::Attack
  throttle('refresh_token/ip', limit: 5, period: 60.seconds) do |req|
    req.ip if req.path == '/api/v1/refresh' && req.post?
  end
end

# Enable rack-attack middleware
Rails.application.config.middleware.use Rack::Attack
