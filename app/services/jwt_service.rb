module JwtService

	def encode(payload, exp = 24.hours.from_now)
    payload[:exp] = exp.to_i
    JWT.encode(payload, Rails.application.credentials.jwt_secret_key, 'HS256')
  end

  def generate_refresh_token(user)
    payload = { user_id: user.id, exp: 1.day.from_now.to_i }
    token = JWT.encode(payload, ENV['JWT_SECRET_KEY'])
    user.update(refresh_token: token)
    token
  end

  def decode(token)
    leeway = 5 * 60
    body = JWT.decode(
      token,
      Rails.application.credentials.jwt_secret_key,
      true,
      { algorithm: 'HS256', exp_leeway: leeway }
    )[0]
    HashWithIndifferentAccess.new body
  rescue StandardError
    nil
  end
end