# frozen_string_literal: true

# This controller handles JWT authentication
class Api::V1::AuthenticationController < ApplicationController
  skip_before_action :authenticate_request, only: %i[signup login]
  include JwtService

  # POST /signup
  def signup
    user = User.create!(user_params)
    token = encode(user_id: user.id)
    refresh_token = generate_refresh_token(user)
    render json: { user: { email: user.email }, token: token, refresh_token: refresh_token }, status: :created
  end

  # POST /login
  def login
    user = User.find_by(email: params[:email])
    if user && user.valid_password?(params[:password])
      token = encode(user_id: user.id)
      refresh_token = generate_refresh_token(user)
      render json: { token: token, refresh_token: refresh_token }, status: :ok
    else
      render json: { error: 'Unauthorized' }, status: :unauthorized
    end
  end

  def refresh
    refresh_token = params[:refresh_token]
    begin
      decoded = decode(refresh_token)
      user = User.find_by(id: decoded[:user_id], refresh_token: refresh_token)
      if user
        # check if the refresh token is not expired.
        new_token = encode(user_id: user.id)
        new_refresh_token = generate_refresh_token(user)
        render json: { token: new_token, refresh_token: new_refresh_token }, status: :ok
      else
        render json: { error: 'Invalid token' }, status: :unauthorized
      end
    rescue JWT::DecodeError => e
      render json: { error: 'Invalid token', details: e.message }, status: :unauthorized
    end
  end

  def logout
    if @current_user
      @current_user.update(refresh_token: nil)
      render json: { message: 'Loggedout successfully!' }, status: :ok
    else
      render json: { error: 'unauthorized!' }, status: :unauthorized
    end
  end

  private

  def user_params
    params.permit(:email, :password, :password_confirmation)
  end
end
