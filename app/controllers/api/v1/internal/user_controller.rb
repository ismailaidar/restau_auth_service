class Api::V1::Internal::UserController < InternalController
	before_action :authenticate_service
  include JwtService
  
    # GET /api/v1/internal/users
    def index
      users = User.all
      render json: users
    end

    # GET /api/v1/internal/users/:id
    def show
      user = User.find(params[:id])
      render json: user
    rescue ActiveRecord::RecordNotFound
      render json: { error: 'User not found' }, status: :not_found
    end

    private

    def authenticate_service
      unless valid_authentication?
        render json: { error: 'Unauthorized' }, status: :unauthorized
      end
    end

    def valid_authentication?
      token = request.headers['Authorization']&.split(' ')&.last
      return false unless token


    end
end